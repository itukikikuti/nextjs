import Link from "next/link";
import Document, { Head, Main, NextScript } from "next/document";
import "../static/style.scss";

export default class MyDocument extends Document {
    render() {
        return (
            <html lang="ja" prefix="og: http://ogp.me/ns#">
                <Head>
                    <meta name="theme-color" content="#0066FF" />
                    <meta name="viewport" content="width=device-width,initial-scale=1" />
                    <link rel="icon" href="/static/icon.png" />
                    <link rel="stylesheet" href="/static/normalize.css" />
                    <link rel="stylesheet" href="/_next/static/style.css" />
                </Head>
                <body>
                    <header>
                        <h1>
                            <Link href="/">
                                <a>windblow</a>
                            </Link>
                        </h1>
                        <small>© 2017 windblow</small>
                        <nav>
                            <ul>
                                <li>
                                    <Link href="/">
                                        <a>トップ</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/StickyCat">
                                        <a>Sticky Cat</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/blog">
                                        <a>ブログ</a>
                                    </Link>
                                </li>
                            </ul>
                        </nav>
                    </header>
                    <Main />
                    <NextScript />
                </body>
            </html >
        );
    }
}
